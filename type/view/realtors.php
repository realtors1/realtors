<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function my_autoloader($users)
{
    include_once '../class/' . $users . '.php';
}

spl_autoload_register('my_autoloader');

if (isset($_POST['realtors'])) {
    $dir_file = '../user/' . $_SESSION['userLogin']['login'] . '/';
    $uploadFile  = $dir_file . basename($_FILES['file']['name']);
if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile)) {
    $object = new real_estate();
    $object->creatHome($_POST);
}
}
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/Style.css" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Загрузка обьектов</title>
</head>
<body>
<header>
    <h1>Загрузка обьектов</h1>
</header>
<div class="container">
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
            <form action="realtors.php" method="post" enctype="multipart/form-data">
                <input type="text" name="street" placeholder="Улица">
                <input type="text" name="number_house" placeholder="Введите номер дома">
                <input type="number" name="number_flat" placeholder="Введите номер квартиры">
                <input type="text" name="price" placeholder="Введите цену">
                <textarea style="width: 400px; height: 200px" name="text" placeholder="Введите описание"></textarea>
                <label><input type="checkbox" name="status">Активный</label>
                <input type="file" name="file">
                <button type="submit" class="btn btn-primary" name="realtors">Отправить</button>
                <a href="../../index.php">Назад</a>
            </form>
        </div>
        <div class="col-sm"></div>
    </div>
</div>
</body>
</html>
