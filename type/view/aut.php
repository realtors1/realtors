<?php

session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function my_autoloader($users)
{
    include_once '../class/' . $users . '.php';
}

spl_autoload_register('my_autoloader');

if (isset($_POST['aut'])) {
    $check = new user();
    $check->check($_POST);
    $_SESSION['userLogin'] = ['login' => $_POST['login']];
}
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/Style.css" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Авторизация</title>
</head>
<body>
<header>
    <h1>Авторизация</h1>
</header>
<div class="container">
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
            <form action="aut.php" method="post">
                <input type="text" name="login" placeholder="Введите логин">
                <input type="password" name="password" placeholder="Введите пароль">
                <button type="submit" class="btn btn-primary" name="aut">Войти</button>
                <a href="../../index.php">Назад</a>
                <?php if (isset($_SESSION['message'])) {
                    echo '<div class="btn btn-outline-danger">' . $_SESSION['message'] . '</div>';
                }
                unset($_SESSION['message']);?>
            </form>
        </div>
        <div class="col-sm"></div>
    </div>
</div>
</body>
</html>
