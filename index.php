<?php

session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function my_autoloader($users)
{
    include_once 'type/class/' . $users . '.php';
}

spl_autoload_register('my_autoloader');
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="type/css/Style.css" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Недвижимости</title>
</head>
<body>
<header>
    <h1>Недвижимости</h1>
    <?php
    if (isset($_SESSION['userLogin']['login'])) {
        echo '<a href="type/view/out.php">Выйти из аккаунта</a>';
    } else {
        echo '<nav><a href="type/view/aut.php">Войти</a> ';
        echo '<a href="type/view/regUser.php">Зарегестрироваться как пользовитель</a> ';
        echo '<a href="type/view/regRealtors.php">Зарегестрироваться как Риэлтор</a></nav>';
    }
    if (isset($_SESSION['admin']['login'])) {
        echo ' <a href="type/view/realtors.php">Загрузить обьект</a>';
    }
    ?>
</header>
<div class="container">
    <div class="row">
        <div class="col-md">
            <?php
            $realtors = new real_estate();
            $realtor = $realtors->listHome();
            foreach ($realtor as $value) {
                if ($value['status'] === 0) {
                    $status = 'Не активно';
                } else {
                    $status = 'Активно';
                }
                echo '<figure>
                        <p><img src="' . $value['file'] .   '" alt="" /></p>
                        <figcaption>Улица ' . $value['street'] .   '</figcaption>
                        <figcaption>Номер дома ' . $value['number_house'] .   '</figcaption>
                        <figcaption>Номер квартиры ' . $value['number_flat'] .   '</figcaption>
                        <figcaption>Цена ' . $value['price'] .   '</figcaption>
                        <figcaption>Статус ' . $status .   '</figcaption>
                       </figure>';
            }
            ?>
        </div>
    </div>
</div>
<footer>

</footer>
</body>
</html>
