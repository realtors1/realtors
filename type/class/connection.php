<?php

class connection
{
    public $connection;

    public function __construct()
    {
        try {
            $this->connection = new PDO("mysql:host=localhost;dbname=realtors;charset=utf8", "root", "root");
        } catch (PDOException $exception) {
            echo $exception ->getMessage();
        }
    }
}