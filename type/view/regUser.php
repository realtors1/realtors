<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function my_autoloader($users)
{
    include_once '../class/' . $users . '.php';
}

spl_autoload_register('my_autoloader');

if (isset($_POST['aut'])) {
    if ($_POST['password'] === $_POST['password2']) {
        $realtor = new user();
        $realtor->creatLoginUser($_POST);
        $_SESSION['user'] = ['login' => $_POST['login']];
        header('Location: ../../index.php');
    }
}
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/Style.css" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Регистрация пользовителя</title>
</head>
<body>
<header>
    <h1>Регистрация пользовителя</h1>
</header>
<div class="container">
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
            <form action="regUser.php" method="post">
                <input type="text" name="login" placeholder="Введите логин">
                <input type="email" name="email" placeholder="Введите почту">
                <input type="password" name="password" placeholder="Введите пароль">
                <input type="password" name="password2" placeholder="Повторите пароль">
                <button type="submit" class="btn btn-primary" name="aut">Зарегестрироватся</button>
            </form>
        </div>
        <div class="col-sm"></div>
    </div>
</div>
</body>
</html>