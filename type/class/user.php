<?php

class user extends connection
{
    public function creatLoginUser($user)
    {
        try {
            $password = md5($_POST['password']);
            $role = 0;
            $statement = $this->connection->prepare("INSERT INTO users (login, password, email, role) VALUE (:login, :password, :email, :role)");
            $statement->execute(array('login' => $_POST['login'], 'password' => $password, 'email' => $_POST['email'], 'role' => $role));
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createLoginRealtor($realtor)
    {
        try {
            $password = md5($_POST['password']);
            $role = 1;
            $statement = $this->connection->prepare("INSERT INTO users (login, password, email, role) VALUE (:login, :password, :email, :role)");
            $statement->execute(array('login' => $_POST['login'], 'password' => $password, 'email' => $_POST['email'], 'role' => $role));
            mkdir('../' . $_POST['login'], 0700);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function updateLogin($user)
    {
        $password = md5($_POST['password']);
        try {
            $statement = $this->connection->prepare('UPDATE users SET password = :password WHERE login = :login AND email = :email');
            $statement->execute(array( 'password' => $password,'login' => $_POST['login'], 'email' => $_POST['email']));
            $statement->fetch(PDO::FETCH_ASSOC);
            if (!empty($user)) {
                if ($_POST['login'] === $user['login']) {
                    if ($_POST['email'] === $user['email']) {
                        header('location: aut.php');
                    }
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function deleteLogin($user)
    {
        try {
            $statement = $this->connection->prepare('DELETE FROM users WHERE login = :login AND email = :email');
            $statement->execute(array('login' => $_POST['login'], 'email' => $_POST['email']));
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function check($login)
    {
        try {
            $password = md5($_POST['password']);
            $statement = $this->connection->prepare('SELECT * FROM users WHERE login = :login AND password = :password LIMIT 1');
            $statement->execute(array('login' => $_POST['login'], 'password' => $password));
            $user = $statement->fetch(PDO::FETCH_ASSOC);
            if (!empty($user)) {
                if ($_POST['login'] === $user['login']) {
                    if ($password === $user['password']) {
                        header('location: ../../index.php');
                        $_SESSION['user'] = ["login" => $_POST['login']];
                        $_SESSION['id'] = ["id" => $user['id']];
                        if ($user['role'] > 0) {
                            $_SESSION['admin'] = ['login' => $_POST['login']];
                        }
                    }
                }
            } else {
                $_SESSION['message'] = 'Логин или пароль не правильный';
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
