<?php

class real_estate extends connection
{
    public function creatHome($object)
    {
        try {
            if (empty($_SESSION['status'])) {
                $status = 1;
            } else {
                $status = 0;
            }
            $statement = $this->connection->prepare("INSERT INTO realtor (street, number_house, number_flat, price, text, status, file, user_id) VALUE (:street, :number_house, :number_flat, :price, :text, :status, :file, :user_id)");
            $statement->execute(array('street' => $_POST['name'], 'number_house' => $_POST['number_house'], 'number_flat' => $_POST['number_flat'], 'price' => $_POST['price'], 'text' => $_POST['text'], 'status' => $status, 'file' => $_FILES['file']['name'],  'user_id' => $_SESSION['id']['id']));
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function listLoginRealtor()
    {
        try {
            $statement = $this->connection ->prepare("SELECT realtor.*, users.* FROM users LEFT JOIN realtor ON users.id=realtor.user_id");
            $statement->execute();
            return $statement->fetchAll();
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function listHome()
    {
        try {
            $statement = $this->connection ->prepare("SELECT * FROM realtor");
            $statement->execute();
            return $statement->fetchAll();
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}